alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

if type -p docker &>/dev/null; then
	alias pandoc='docker run --rm -i --volume "${PWD}:/pandoc" dalibo/pandocker'
	alias bacasable='docker run --rm -it -v ${PWD}:/etabli -w /etabli -e COLORTERM -e TERM -e HISTFILE=/etabli/mon-historique-bash-bacasable -e LANG=C.UTF-8 -e http_proxy --entrypoint $SHELL'
	alias docker-compose="docker compose"
fi

alias lxc-start='systemd-run --user --remain-after-exit --property Delegate=yes lxc-start'
alias lxc-attach='lxc-attach --clear-env -v HOME=/root -v LANG=C.UTF-8 --keep-var TERM --keep-var TMUX'

### COLORATION ls, grep
if [ -x /usr/bin/dircolors ]; then
	eval "$(dircolors -b)"
	export LS_COLORS="$LS_COLORS:ow=1;7;34:st=30;44:su=30;41"
	alias ls='ls --color=auto'
	alias grep='grep --color=auto'
fi

if type -p exa &>/dev/null; then
	alias ls="exa --group-directories-first --icons --classify"
fi

if type -p bat &>/dev/null; then
	alias less="bat --theme=gruvbox-dark"
fi

### COMPLÉTION
export INPUTRC="${HOME}/.config/inputrc"
for f in "$BASHRC_HOME/.local/lib/bash_completion.d"/*.bash; do
	if [ -e "$f" ]; then
		# shellcheck source=/dev/null
		. "$f"
	fi
done
unset f
